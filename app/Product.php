<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [

        'category_id',
        'vendor',
        'title',
        'code',
        'description',
        'price',
        'warranty',
        'in_stock',
    ];



    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
