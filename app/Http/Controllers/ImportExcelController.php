<?php


namespace App\Http\Controllers;


use App\Category;
use App\Imports\CategoryImport;
use App\Imports\ProductsImport;
use App\Product;
use App\Repository\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\ImportExlFileRequest;
use Maatwebsite\Excel\Validators\ValidationException;

/**
 * Class ImportExcelController
 * @package App\Http\Controllers
 */
class ImportExcelController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = DB::table('products')
            ->select('products.*', 'categories.category_name')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->paginate(5);

        $nums = ProductRepository::countAllProducts();

        return view('import_exl', ['products' => $products, 'nums' => $nums]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function import(ImportExlFileRequest $request)
    {
        // Setup Max Execution time
        ini_set('max_execution_time', 11);

        // Import data from Excel
        try {
            $products_data = Excel::import(new CategoryImport(), request()->file('excel_file'))->queueImport(new ProductsImport(), request()->file('excel_file'));
        } catch (ValidationException $e) {
            return response()(['no-success' => 'errorList', 'message' => $e->errors()]);
        }

        if ($products_data) {
            return back()->with('success', 'Excel Data Imported successfully.');
        } else {
            return back()->with('no-success', 'Cant import');
        }
    }


    /**
     * Truncate Product model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete()
    {
        if (Product::all(['product_id'])->count() && Category::all(['id'])->count() > 0) {
            Product::query()->delete();
            Category::query()->delete();
            return back()->with('success', 'Excel Data Deleted successfully.');
        } else {
            return back()->with('no-success', 'Nothing to Delete.');
        }
    }

}
