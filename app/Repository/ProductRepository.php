<?php


namespace App\Repository;

use App\Product;
use Carbon\Carbon;


class ProductRepository
{

    CONST CACHE_KEY = 'prd';

    public static function countAllProducts()
    {
        return cache()->remember(
            self::CACHE_KEY,
            Carbon::now()->addMinutes(5),
            function () {
                return Product::all(['product_id'])->count();
            }
        );
    }

    public function getCacheKey()
    {
        return self::CACHE_KEY;
    }

}
