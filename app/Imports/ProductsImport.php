<?php

namespace App\Imports;

use App\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ProductsImport implements ToModel, WithValidation, SkipsOnFailure, WithStartRow, ShouldQueue, WithChunkReading
{
    use Importable, SkipsFailures;


    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function chunkSize(): int
    {
        return 1000;
    }


    public function batchSize(): int
    {
        return 500;
    }


    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $product = new Product();
        $product->category_id = 1;
        $product->vendor = $row[2];
        $product->title = $row[3];
        $product->code = $row[4];
        $product->description = $row[5];
        $product->price = $row[7];
        $product->warranty = $row[8];
        $product->in_stock = $row[9];
        $product->save();

        return $product;
    }

    public function onFailure(Failure ...$failures)
    {
        // TODO: Implement onFailure() method.
    }


    public function rules(): array
    {
        return [

//            '3' => Rule::unique('products', 'title'),
        ];
    }


    public function customValidationMessages()
    {
        return [
            '3.unique' => 'Duplicates present',
        ];
    }

    /**
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        \Log::error($e->getMessage());

        return back();
    }
}
