<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\DB;
Route::get('/', function () {
//    return view('welcome');
//    $db =  DB::table('test')->select('*')->get();
//dd($db);
    //    return \Illuminate\Support\Facades\Response::
});

Route::get('/import_excel', 'ImportExcelController@index');
Route::post('/import_excel/import', 'ImportExcelController@import');
Route::get('/product/delete', 'ImportExcelController@delete')->name('product.delete');
